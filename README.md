# Revelation

Reads an [Revelation][] XML export and dumps it as CSV to _STDOUT_.

[revelation]: https://revelation.olasagasti.info/

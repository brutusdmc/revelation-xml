#!/usr/bin/env python3
"""
Reads an *Revelation* XML export and dumps it as CSV to STDOUT.

"""
import argparse
import csv
import datetime as dt
import json
import sys
from xml.etree import ElementTree


CSV_HEADER = [
    "folder",
    "favorite",
    "type",
    "name",
    "notes",
    "fields",
    "login_uri",
    "login_username",
    "login_password",
    "login_totp",
]

FIRST_FIELDS = {
    "name": ("name", "keyfile", "location"),
    "login_uri": ("hostname", "url", "location"),
    "login_username": ("username", "email", "keyfile"),
    "login_password": ("password", "code"),
}

MERGE_FIELDS = {
    "notes": ("notes", "description"),
}

IGNORE_EXTRA_FIELDS = set(("hostname", "username", "password", "description"))


def parse_args(argv=None):
    ap = argparse.ArgumentParser(description="Dump Revelation XML as CSV")
    ap.add_argument("filename", metavar="FILE", help="path to XML file")
    ap.add_argument(
        "-f", "--output-format",
        choices=("raw", "json", "csv"),
        help="set output format (default: %(default)s)",
    )
    ap.add_argument(
        "--auto-fix", action="store_true", help="try to handle missing XML data"
    )
    ap.set_defaults(output_format="csv",)
    args = ap.parse_args(argv)
    return args


def parse_xml(filename):
    return ElementTree.parse(filename).getroot()


def first_value(entry, *keys):
    """ Return first non-empty values of *keys* from *entry*.

    >>> entry = dict(bug="", foo="bar", x=0, bar="baz")
    >>> first_value(entry, "bug", "bar", "foo")
    'baz'

    """
    values = (entry.get(k, "").strip() for k in keys)
    values = (v for v in values if v)
    try:
        return list(values)[0]
    except IndexError:
        msg = f"no content found under these keys: {', '.join(keys)}"
        raise ValueError(msg)


def merge_values(entry, *keys, joiner="\n\n"):
    """ Return *joined* values of *keys* from *entry*.

    >>> entry = dict(bug="", foo="bar", x=0, bar="baz")
    >>> merge_values(entry, "bug", "bar", "foo")
    'baz\\n\\nbar'

    """
    values = (entry.get(k, "").strip() for k in keys)
    values = (v for v in values if v)
    return joiner.join(values).strip()


def fields_to_string(entry, *keys, seperator=": ", joiner="\n"):
    """ Return key and value merged by *seperator* for all *keys* joined.

    >>> entry = dict(foo="bar", x=0, bar="baz")
    >>> fields_to_string(entry, "bar", "foo")
    'bar: baz\\nfoo: bar'

    """
    return joiner.join([f"{k}{seperator}{entry[k]}" for k in keys])


def entry_to_dict(entry, auto_fix=False):
    """ Return XML *entry* as dictionary. """
    d = {
        "type": entry.get("type"),
    }
    try:
        for element in entry:
            if element.tag == "field":
                key = element.get("id")
                if key.startswith("generic-"):
                    key = key[8:]
                d[key] = element.text or ""
            else:
                d[element.tag] = element.text or ""

        if "updated" in d:
            stamp = dt.datetime.fromtimestamp(int(d["updated"]))
            d["updated"] = stamp.isoformat()

        if auto_fix:

            if d["type"] == "website":
                if not d.get("email"):
                    d["email"] = d["username"]
                elif not d.get("username"):
                    d["username"] = d["email"]
                if not d.get("hostname"):
                    d["hostname"] = d["url"]

            if d["type"] == "email":
                if not d.get("username"):
                    d["username"] = d["email"]
                elif not d.get("email"):
                    d["email"] = d["username"]
                if not d.get("hostname"):
                    d["hostname"] = d["email"].split("@")[1]

            if d["type"] == "creditcard":
                if not d.get("hostname"):
                    d["hostname"] = d["name"]
                if not d.get("username"):
                    d["username"] = d["creditcard-cardnumber"]
                if not d.get("password"):
                    d["password"] = d["pin"]

            if d["type"] == "phone":
                if not d.get("hostname"):
                    d["hostname"] = d["name"]
                if not d.get("username"):
                    d["username"] = d["phone-phonenumber"]
                if not d.get("password"):
                    d["password"] = d["pin"]

            if d["type"] == "cryptokey":
                if not d.get("username"):
                    d["username"] = d["name"]

            if d["type"] == "door":
                if not d.get("username"):
                    d["username"] = d["name"]

        return d
    except:
        print("-" * 78)
        print(d)
        print("-" * 78)
        raise


def dict_to_bitwarden(
    entry,
    add_extra_fields=True,
    totp=None,
    favorite=None,
    e_type="login",
    folder="REVELATION",
    first_fields=FIRST_FIELDS,
    merge_fields=MERGE_FIELDS,
    ignore_extra_fields=IGNORE_EXTRA_FIELDS,
):
    """ Return dictonary entry in Bitwarden format. """
    d = {
        "type": e_type,
        "folder": folder,
        "favorite": favorite,
        "login_totp": totp,
        "fields": "",
    }
    try:
        for field, keys in first_fields.items():
            d[field] = first_value(entry, *keys)

        for field, keys in merge_fields.items():
            d[field] = merge_values(entry, *keys)

        if add_extra_fields:
            extra_fields = {key for key in entry if key not in CSV_HEADER}
            extra_fields = extra_fields - ignore_extra_fields
            if extra_fields:
                d["fields"] = fields_to_string(entry, *extra_fields)

        return d
    except:
        print("-" * 78)
        print(entry)
        print("-" * 78)
        raise


def get_entries(element):
    """ Return all `entry` elements in *element*.

    Except `entry` elements of `type="folder"`, they are recursivly
    searched for more entries instead.

    """
    if element.tag == "revelationdata":
        for e in element:
            yield from get_entries(e)
    else:
        if element.tag == "entry":
            if element.get("type") == "folder":
                for e in element:
                    yield from get_entries(e)
            else:
                yield element


def get_dicts(entries, **kwargs):
    return (entry_to_dict(e, **kwargs) for e in entries)


def get_data(dicts):
    return (dict_to_bitwarden(d) for d in dicts)


def dump_csv(entries, file=sys.stdout, fieldnames=CSV_HEADER):
    writer = csv.DictWriter(
        sys.stdout, fieldnames=fieldnames, quoting=csv.QUOTE_ALL, strict=True
    )
    writer.writeheader()
    writer.writerows(entries)


def main(filename, output_format="csv", auto_fix=False):
    root = parse_xml(filename)
    entries = get_dicts(get_entries(root), auto_fix=auto_fix)

    if output_format == "raw":
        entries = list(entries)
        json.dump(entries, sys.stdout, sort_keys=True, indent=2)
        print()
        types = {e.get("type") for e in entries}
        print(f"Types: {len(types)} {', '.join(sorted(types))}", file=sys.stderr)
        print(f"Count: {len(entries)}", file=sys.stderr)
        return 0

    entries = get_data(entries)

    if output_format == "json":
        entries = list(entries)
        json.dump(entries, sys.stdout, sort_keys=True, indent=2)
        print()
        return 0

    dump_csv(entries)
    return 0


if __name__ == "__main__":
    args = parse_args()
    sys.exit(main(**vars(args)))
